from flask import Blueprint, render_template, redirect, url_for
from project import admin
from project.models import db, User
from flask_login import current_user

from flask_admin import expose, AdminIndexView
from flask_admin.contrib.sqla import ModelView

mod = Blueprint('second', __name__)

class MicroBlogModelView(ModelView):

    def is_accessible(self):
        return current_user.is_authenticated

    def inaccessible_callback(self, name, **kwargs):
        # redirect to login page if user doesn't have access
        return redirect(url_for('login', next=request.url))

class MyAdminIndexView(AdminIndexView):
    def is_accessible(self):
        return current_user.is_authenticated() # This does the trick rendering the view only if the user is authenticated

admin.name='Test administration'
admin.template_mode='bootstrap3'
admin.index_view = MyAdminIndexView()

admin.add_view(MicroBlogModelView(User, db.session))

@mod.route('/second')
def index():
    return render_template('second.html')