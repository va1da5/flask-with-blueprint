from flask import Blueprint, render_template
from project.models import db, User
from flask_security import login_required
mod = Blueprint('first', __name__)


@mod.route('/')
def home():
	return render_template('index.html')

@mod.route('/first')
@login_required
def first():
	users = User.query.all()
	return render_template('first.html', users=users)