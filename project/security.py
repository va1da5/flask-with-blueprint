from project import app
from project.models import db, User, Role
from flask_security import Security, SQLAlchemyUserDatastore

# Setup Flask-Security
user_datastore = SQLAlchemyUserDatastore(db, User, Role)
security = Security(app, user_datastore)