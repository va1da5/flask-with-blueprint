from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_admin import Admin


app = Flask(__name__)

app.config['SECRET_KEY'] = 'super-secret'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///test.db'
app.config['SECURITY_PASSWORD_SALT'] = 'random text here'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

admin = Admin(app)

db = SQLAlchemy(app)

from project.security import *
# Create a user to test with
# @app.before_first_request
# def create_user():
#     db.create_all()
#     user_datastore.create_user(email='vaidas@mail.com', password='password')
#     db.session.commit()


from project.views import first
from project.views import second

app.register_blueprint(first.mod)
app.register_blueprint(second.mod)